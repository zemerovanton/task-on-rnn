# Task on RNN

## Task1,2
Notebook is about creating character-level language model using RNN. 
Threre are examples of using GRU units.

## Task3
Notebook is about text classification on IMDB corpora. 
The model`s output is predicted probabolities of each class.

* GRU accuracy: 0.77
* LSTM accuracy: 0.80

## Task4
Task is the same as task3 but model based on convolutional layers. 

* ConvNet accuracy: 0.81